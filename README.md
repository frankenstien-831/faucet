[![banner](https://raw.githubusercontent.com/oceanprotocol/art/master/github/repo-banner%402x.png)](https://oceanprotocol.com)

<h1 align="center">Faucet Server</h1>

> 🚰 The Ocean Faucet Server allows users to request Ether for a particular Ethereum network.
> A UI for it is deployed under https://commons.oceanprotocol.com/faucet

[![Build Status](https://travis-ci.com/oceanprotocol/faucet.svg?branch=master)](https://travis-ci.com/oceanprotocol/faucet)
![Docker Cloud Build Status](https://img.shields.io/docker/cloud/build/oceanprotocol/faucet.svg)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-7b1173.svg)](https://github.com/prettier/prettier)
[![js oceanprotocol](https://img.shields.io/badge/js-oceanprotocol-7b1173.svg)](https://github.com/oceanprotocol/eslint-config-oceanprotocol)

---

**Table of Contents**

- [Prerequisites](#prerequisites)
- [Get Started](#get-started)
- [Usage](#usage)
  - [`POST /faucet`](#post-faucet)
  - [`GET /`](#get-)
  - [UI Example](#ui-example)
- [Configuration](#configuration)
- [Testing](#testing)
- [Production build](#production-build)
- [Deployment](#deployment)
- [Releases](#releases)
- [Contributing](#contributing)
- [License](#license)

---

## Prerequisites

- Node.js v12 or later
- [Docker](https://www.docker.com/get-started)
- [Docker Compose](https://docs.docker.com/compose/)
- [Barge](https://github.com/oceanprotocol/barge)

## Get Started

Before starting to develop, you need to run a local Ocean network using [Barge](https://github.com/oceanprotocol/barge):

```bash
git clone https://github.com/oceanprotocol/barge.git
cd barge
./start_ocean.sh --no-secret-store --no-aquarius --no-brizo --no-events-handler --no-commons --no-faucet
```

Then, start the Faucet server in a live-reloading watch mode:

```bash
docker-compose up -d mongo
npm install
npm start
```

## Usage

### `POST /faucet`

To request Ether, a user can send an HTTP POST request to `http://localhost:3001/faucet` with an HTTP request body like:

```js
{
    "address": "<string>", //required
    "agent": "<string>",   //optional, Possible values - server, twitter, telegram, gitter
}
```

An example HTTP POST request using `wget`:

```bash
wget --header="Content-Type: application/json" \
--post-data '{"address": "0x7E187af69973a66e049a15E763c97CB726765f87", "agent": "twitter"}' \
http://localhost:3001/faucet
```

Sample Response Body:

```json
{
  "success": true,
  "message": "Successfully added 3 ETH to your account.",
  "txHash": "0xb86e762b870055deee924aeb33c08c162abdb6a71faa8531dbba84e985402b64"
}
```

An error response looks like this:

```json
{
  "success": false,
  "message": "Crypto is hard."
}
```

### `GET /`

Shows information about the software version, which will be returned as HTML by default to be more human-friendly when accessed from a browser.

To get a json response instead, set the `Accept` header to `application/json`:

```bash
wget --header="Accept: application/json" \
http://localhost:3001/
```

Sample Response Body:

```json
{
  "software": "faucet",
  "version": "0.2.2",
  "network": "Pacific",
  "keeper": "https://pacific.oceanprotocol.com"
}
```

### UI Example

For an example on how to implement a UI, have a look at the React component in commons:

- [commons:client/src/routes/Faucet.tsx](https://github.com/oceanprotocol/commons/blob/master/client/src/routes/Faucet.tsx)

## Configuration

You can configure the server in the file [src/config.js](src/config.js) or by exporting values into your environment.

The amount of Ether is determined by the values of the configuration settings `server.faucetEth`.

To connect remotely to Ocean's Nile network instead of a local running network, modify the `server.faucetNode` setting like so:

```js
server: {
  ...,
  faucetNode: 'https://nile.dev-ocean.com'
}
```

The timespan check can be disabled by setting `faucetTimeSpan` to `0`:

```js
server: {
  ...,
  faucetTimeSpan: 0
}
```

## Testing

Before running the tests, configure your test environment variables if needed and make sure MongoDB service is running:

```bash
cp tests/.env.test.example tests/.env.test
docker-compose up -d mongo
```

Then start the unit tests:

```bash
npm test
```

To get a test coverage report:

```bash
npm run coverage
```

## Production build

To create a production build of the Faucet server, execute:

```bash
npm run build
```

This will create the build output into `./dist`. You can then run and serve from this build output with:

```bash
npm run serve
```

## Deployment

You can deploy the Faucet server using `docker-compose`. The Docker image will do a production build of the server and run it:

```bash
docker-compose up
```

## Releases

Running any release task does the following:

- bumps the project version
- creates a Git tag
- updates CHANGELOG.md file with commit messages
- commits and pushes everything
- creates a GitHub release with commit messages as description

You can execute the script using {major|minor|patch} as first argument to bump the version accordingly:

- To bump a patch version: `npm run release`
- To bump a minor version: `npm run release minor`
- To bump a major version: `npm run release major`

By creating the Git tag with these tasks, Travis will trigger a new Kubernetes deployment automatically aftr a successful tag build.

For the GitHub releases steps a GitHub personal access token, exported as `GITHUB_TOKEN` is required. [Setup](https://github.com/release-it/release-it#github-releases)

## Contributing

See the page titled "[Ways to Contribute](https://docs.oceanprotocol.com/concepts/contributing/)" in the Ocean Protocol documentation.

## License

```text
Copyright 2018 Ocean Protocol Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
